document.addEventListener('DOMContentLoaded', function () {
  console.log('Script loaded');
  var audio = document.getElementById('audioElement');
  var playPauseBtn = document.getElementById('playPauseBtn');
  var playIcon = document.querySelector('.play-icon');
  var pauseIcon = document.querySelector('.pause-icon');
  var typedInstances = [];
  var isAudioPlaying = false;
  var audioProgress = document.getElementById('audioProgress');
  var hamburger = document.querySelector('.hamburger');
  var navUl = document.querySelector('nav ul');

  // Function to initialize or resume Typed.js animations
  function initTypedAnimations() {
      if (typedInstances.length === 0) {
          typedInstances.push(new Typed('.p-typed-arabic1', {
              stringsElement: '#arabic-content',
              typeSpeed: 40,
              showCursor: false,
          }));

          typedInstances.push(new Typed('.p-typed-english1', {
              stringsElement: '#english-content',
              typeSpeed: 40,
              showCursor: false,
          }));
      } else {
          typedInstances.forEach(function (instance) {
              instance.start();
          });
      }
  }

  

  // Function to pause Typed.js animations
  function pauseTypedAnimations() {
      typedInstances.forEach(function (instance) {
          instance.stop();
      });
  }

  // Function to update the audio progress bar
  function updateProgress() {
      if (audio && audio.duration) {
          audioProgress.value = audio.currentTime / audio.duration;
      }
  }

  // Function to reset Typed.js animations and audio
  function resetTypedAnimations() {
      typedInstances.forEach(function (instance) {
          instance.reset(true);
      });
      audio.currentTime = 0;
      playIcon.style.display = 'inline';
      pauseIcon.style.display = 'none';
      isAudioPlaying = false;
  }

  // Event listeners for the play/pause button
  playPauseBtn.addEventListener('click', function () {
      if (audio.paused || audio.ended) {
          audio.play();
          playIcon.style.display = 'none';
          pauseIcon.style.display = 'inline';
          isAudioPlaying = true;
          initTypedAnimations();
      } else {
          audio.pause();
          playIcon.style.display = 'inline';
          pauseIcon.style.display = 'none';
          isAudioPlaying = false;
          pauseTypedAnimations();
      }
  });

  // Event listener for the reset button
  var resetBtn = document.getElementById('resetBtn');
  if (resetBtn) {
      resetBtn.addEventListener('click', function () {
          resetTypedAnimations();
          if (isAudioPlaying) {
              playPauseBtn.click();
          }
      });
  } else {
      console.error('Reset button not found');
  }

  // Event listener for the hamburger menu
  if (hamburger && navUl) {
      hamburger.addEventListener('click', function () {
          navUl.classList.toggle('show');
      });
  }

  // Update the progress bar as the audio plays
  audio.addEventListener('timeupdate', updateProgress);

  // Event listeners for the audio element to start/stop Typed.js animations
  audio.addEventListener('play', initTypedAnimations);
  audio.addEventListener('pause', pauseTypedAnimations);
});
