# Syrian memories

My final year project is a website designed as a digital archive to capture and showcase the rich cultural heritage of Syria through curated podcasts and interviews. Focused on personal experiences, the platform seeks out existing online content where individuals share their cherished memories and stories related to Syria. 

The immersive website, artistically designed and easily navigable, categorizes these narratives by themes and regions, offering a multi-sensory experience with embedded audio, video, and images. With translations provided, the "Memories of Syrians" aims to preserve Syrian culture, memories, and history, ensuring the resilience of cultural heritage amid the ongoing crisis. As a Syrian in the diaspora with a background in art, design, and programming, the project serves as both a personal connection to Syria and an opportunity to apply and enhance skills, contributing positively to the community.


link to website: https://syrianmemories.life/ 

<br>
(Final documents including Final Report, Presentation, Weekly Log, all are in the "Final Docs" Folder)